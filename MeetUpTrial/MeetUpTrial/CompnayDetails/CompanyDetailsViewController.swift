//
//  CompanyDetailsViewController.swift
//  MeetUpTrial
//
//  Created by devika.ramadasi on 08/08/20.
//  Copyright © 2020 Sushma S. All rights reserved.
//

import UIKit

class CompanyDetailsViewController: UIViewController {
    @IBOutlet weak var symbod: UILabel?
    
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var industry: UILabel?
    @IBOutlet weak var name: UILabel?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    var companyNAme = "IBM"
    var companyDetailsViewModel = CompanyDetailsViewModel()
    class func loadViewController() -> CompanyDetailsViewController? {
        let storyboard = UIStoryboard(name: "CompanyDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CompanyDetailsViewController") as! CompanyDetailsViewController
        return vc

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        companyDetailsViewModel.selectedCompany = companyNAme
        activityIndicator?.startAnimating()
        companyDetailsViewModel.fetchCompanyDetails {  [weak self] (success) in
            self?.symbod?.text = self?.companyDetailsViewModel.companyInfoModel?.symbol
            self?.name?.text = self?.companyDetailsViewModel.companyInfoModel?.name
            self?.detailsLabel?.text = self?.companyDetailsViewModel.companyInfoModel?.description
            self?.industry?.text = self?.companyDetailsViewModel.companyInfoModel?.industry
            self?.activityIndicator?.stopAnimating()

        }
        // Do any additional setup after loading the view.
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
