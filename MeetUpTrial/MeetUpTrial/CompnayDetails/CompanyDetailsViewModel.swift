//
//  CompanyDetailsViewModel.swift
//  MeetUpTrial
//
//  Created by devika.ramadasi on 08/08/20.
//  Copyright © 2020 Sushma S. All rights reserved.
//

import Foundation
class CompanyDetailsViewModel {
    var selectedCompany = "BABA"
    var companyInfoModel : CompanyInfoModel?
    
    func fetchCompanyDetails(onCompletion: @escaping ((_ isSuccess: Bool) -> Void))  {
        let CO = "OVERVIEW"
        let timeSeriesDaily = TimeSeriesDailyRequest(symbol: CO)
        let rsi = OverViewRequest(symbol: selectedCompany)
        rsi.callApi { (isSucces) in
            if isSucces , rsi.responseJSON  != nil {
                self.companyInfoModel = CompanyInfoModel(details: rsi.responseJSON!)

            }
            onCompletion(isSucces)

            
    }
        
    }
}
