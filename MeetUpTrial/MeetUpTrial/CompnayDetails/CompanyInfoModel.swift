//
//  CompanyInfoModel.swift
//  MeetUpTrial
//
//  Created by devika.ramadasi on 08/08/20.
//  Copyright © 2020 Sushma S. All rights reserved.
//

import Foundation
class CompanyInfoModel: Codable {
    var symbol : String?
    var assetType : String?
    var name: String?
    var description: String?
    var country: String?
    var industry: String?
    init(details:[String:Any]) {
        if  let symbolInfo = details["Symbol"] as? String{
            symbol = symbolInfo
        }
        if  let AssetType = details["AssetType"] as? String{
                   assetType = AssetType
               }
        if  let Name = details["Name"] as? String{
            name = Name
        }
        if  let Description = details["Description"] as? String{
                   description = Description
               }
              
        if  let Country = details["Country"] as? String{
                          country = Country
        
        }
        
        if  let Industry = details["Industry"] as? String{
                   industry = Industry
               }
    }
}
