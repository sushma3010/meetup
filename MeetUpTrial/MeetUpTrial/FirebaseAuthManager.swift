//
//  FirebaseAuthManager.swift
//  MeetUpTrial
//
//  Created by Sushma S on 08/08/20.
//  Copyright © 2020 Sushma S. All rights reserved.
//

import Foundation
import FirebaseAuth



final class FirebaseAuthManager {
    
    static let shared = FirebaseAuthManager()
    
    private init(){}

    func createUser(email: String, password: String, completionBlock: @escaping (_ success: Bool) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) {(authResult, error) in
            if let user = authResult?.user {
                print(user)
                completionBlock(true)
            } else {
                completionBlock(false)
            }
        }
    }
    
    
    func signinUser(email: String, password: String,completionBlock: @escaping (_ success: Bool) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            if let user = authResult?.user {
               print(user)
               completionBlock(true)
           } else {
               completionBlock(false)
           }
        }
    }
}
