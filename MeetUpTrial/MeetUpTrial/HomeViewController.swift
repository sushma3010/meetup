//
//  HomeViewController.swift
//  MeetUpTrial
//
//  Created by Sushma S on 08/08/20.
//  Copyright © 2020 Sushma S. All rights reserved.
//

import Foundation
import UIKit

class HomeViewController: ViewController {
    @IBOutlet weak var suggestionTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    var searchViewModel = SearchViewModel()
    var displaySearches : [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.suggestionTableView.isHidden = true
        searchViewModel.fetchSuggestions(keywords: "", completion: { (sucess, resultarray) in
            self.displaySearches = resultarray
        })
    }
    @IBAction func bottonButtonTapped(_ sender: Any) {
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let controler = CompanyDetailsViewController.loadViewController()
        {
            self.navigationController?.pushViewController(controler, animated: true)

        }
    }
}

extension HomeViewController : UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
                self.suggestionTableView.isHidden = false
                self.suggestionTableView.reloadData()
                self.displaySearches = []
                self.tableViewHeightConstraint.constant = suggestionTableView.contentSize.height
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        suggestionTableView.isHidden = true
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        suggestionTableView.isHidden = true
        let searchText = searchBar.text
        searchViewModel.fetchSuggestions(keywords: searchText ?? "", completion: { [weak self] (sucess, resultarray) in
            if resultarray.count != 0 {
                self?.suggestionTableView.isHidden = false
                self?.suggestionTableView.reloadData()
                self?.displaySearches = resultarray
                self?.tableViewHeightConstraint.constant = self?.suggestionTableView.contentSize.height as! CGFloat
            }
        })
    }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchViewModel.fetchSuggestions(keywords: searchBar.text ?? "", completion: { [weak self] (sucess, resultarray) in
            if resultarray.count != 0 {
                self?.suggestionTableView.isHidden = false
                self?.suggestionTableView.reloadData()
                self?.displaySearches = resultarray
                self?.tableViewHeightConstraint.constant = self?.suggestionTableView.contentSize.height as! CGFloat
                self?.suggestionTableView.reloadData()

            }
        })
    }
}

extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displaySearches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = displaySearches[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.isHidden = true
        //show some detail screen
    }
}
