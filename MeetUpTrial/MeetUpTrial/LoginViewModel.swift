//
//  LoginViewModel.swift
//  MeetUpTrial
//
//  Created by Sushma S on 08/08/20.
//  Copyright © 2020 Sushma S. All rights reserved.
//

import Foundation

class LoginViewModel {
    
    func registerUser(email: String, password: String, completionBlock: @escaping (_ success: Bool) -> Void) {
        FirebaseAuthManager.shared.createUser(email: email, password: password, completionBlock: completionBlock)
    }
    
    func signInuser(email : String, password: String, completionBlock: @escaping (_ success: Bool)->Void) {
        FirebaseAuthManager.shared.signinUser(email: email, password: password, completionBlock: completionBlock)
    }
    
    func validateEmailAndPassword(email : String, password: String) -> Bool {
        var isEmailValid = false
        var isPasswordValid = false
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        isEmailValid = emailPred.evaluate(with: email)
        
        isPasswordValid = password.count >= 6
        return isEmailValid && isPasswordValid
    }
    
   
}



