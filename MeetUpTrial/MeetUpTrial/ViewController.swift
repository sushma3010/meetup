//
//  ViewController.swift
//  MeetUpTrial
//
//  Created by Sushma S on 08/08/20.
//  Copyright © 2020 Sushma S. All rights reserved.
//


import UIKit
class ViewController: UIViewController {
    
    @IBOutlet weak var searchSuggestionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    var loginViewModel = LoginViewModel()
    var IBMInfoButton : UIButton?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let navRightButton = UIBarButtonItem(title: "IBM", style: UIBarButtonItem.Style.plain, target: self, action: #selector(showDetailPAge))
        self.navigationItem.rightBarButtonItem = navRightButton
    }
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
         
       }
    
    @objc func showDetailPAge()  {
        if let controler = CompanyDetailsViewController.loadViewController()
                 {
                    controler.companyNAme = "GOLD"
                     self.navigationController?.pushViewController(controler, animated: true)

                 }
    }
        
    
    
    @IBAction func signinTapped(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            if loginViewModel.validateEmailAndPassword(email: email, password: password) {
                loginViewModel.signInuser(email: email, password: password) { [weak self] (success) in
                    guard let `self` = self else { return }
                    if (success) {
                        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                    } else {
                        let message = "Error occured while registering"
                        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                }
            } else {
                let alertController = UIAlertController(title: nil, message: "Invalid email and password", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func registerTapped(_ sender: Any) {
        
        if let email = emailTextField.text, let password = passwordTextField.text {
            if loginViewModel.validateEmailAndPassword(email: email, password: password) {
                loginViewModel.registerUser(email: email, password: password) { [weak self] (success) in
                    guard let `self` = self else { return }
                    var message: String = ""
                    if (success) {
                        message = "Registered. You can now signin"
                    } else {
                        message = "Error occured while registering"
                    }
                    let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                let alertController = UIAlertController(title: nil, message: "Invalid email and password", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
}




