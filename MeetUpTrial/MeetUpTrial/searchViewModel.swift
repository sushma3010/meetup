//
//  searchViewModel.swift
//  MeetUpTrial
//
//  Created by Sushma S on 08/08/20.
//  Copyright © 2020 Sushma S. All rights reserved.
//

import Foundation

class SearchViewModel {
    func fetchSuggestions(keywords: String, completion: @escaping (_ success: Bool, _ searchkeywords: [String]) -> Void)  {
        let rsi = FetchSymbols(keyword: keywords)
       _ = rsi.callApi { (isSucces) in
        completion(isSucces, ["IBM","INFOSYs"])
        }
    }
}
